#pragma once

#include <gctypes.h>

u32 asm_B(u32 dest);
u32 asm_BL(void *dest);
u32 asm_BA(void *dest);
u32 asm_BLA(void *dest);
u32 asm_ADDI(u32 destReg, u32 srcReg, u32 addr);
u32 asm_ADDIC(u32 destReg, u32 srcReg, u32 addr);
u32 asm_ADDIS(u32 destReg, u32 srcReg, u32 addr);
u32 asm_LI(u32 r, u32 addr);
u32 asm_LIS(u32 r, u32 addr);
u32 asm_NOP();

void WriteCode(u32 address, u32 instr);
void DetourFunction(u32 address, void *replacementAddr);
