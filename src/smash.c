#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <gctypes.h>

#include <libutils/fs/sd_fat_devoptab.h>
#include <dynamic_libs/os_functions.h>
#include <dynamic_libs/gx2_functions.h>
#include <dynamic_libs/vpad_functions.h>

#include "fs.h"
#include "smash.h"
#include "asm.h"

#include "fighter.h"
#include "article.h"
#include "weapon.h"

extern int (* const entryPoint)(int argc, char **argv);

void applyModsFile (u64 titleId)
{
    char filename[32] = {0};
    sprintf(filename, "sd:/%016llX/codes.bin", titleId);

    // No file? No problem!
    if (!CheckFile(filename))
        return;

    u8 *buf = malloc(0x1000);
    LoadFileToMem(filename, &buf, NULL);

    u32 *code = (u32*)buf;
    u32 magic = *(code++);
    u32 version = *(code++);
    u32 numCodes = *(code++);
    code++; // padding

    for (u32 i = 0; i < numCodes; ++i) {
        u32 codeType = *(code++);
        u32 addr = *(code++);
        u32 numInstrs = *(code++);
        u32 outPtr = addr;

        for (u32 j = 0; j < numInstrs; ++j) {
            WriteCode(outPtr, *(code++));
            outPtr += 4;
        }
    }
}

int isNameAllowed(char *name)
{
    return 1;
}

const char* getRegionName()
{
    return "us_sp";
}

typedef struct
{
    u32 unk00; // 00
    u32 unk01; // 04
    u32 unk02; // 08
    u32 unk03; // 0C
    u32 unk04; // 10
    u32 unk05; // 14
    u32 unk06; // 18
    u32 unk07; // 1C
    u32 unk08; // 20
    u32 unk09; // 24
    u32 unk10; // 28
    u32 unk11; // 2C
    u32 unk12; // 30
    u32 unk13; // 34
    u32 unk14; // 38
    u32 unk15; // 3C
    u32 unk16; // 40
    u32 unk17; // 44
    u32 unk18; // 48
} FighterParamEntry;

typedef struct
{
    // idk yet lol
} UiFighterDatabase;

typedef struct
{
    u32 unk00; // 00
    u32 unk01; // 04
    u32 Series; // 08
    u32 unk03; // 0C
    u32 unk04; // 10
    u32 unk05; // 14
    u32 unk06; // 18
    u32 unk07; // 1C
    u32 unk08; // 20
    u32 MaxFighters; // 24
    u32 MaxFightersOmega; // 28
    u32 unk09; // 2C
    u32 unk10; // 30
    u32 unk11; // 34
    u32 unk12; // 38
    u32 unk13; // 3C
    u32 unk14; // 40
    u32 unk15; // 44
    u32 unk16; // 48
    u32 unk17; // 4C
    u32 unk18; // 50
} StageParamEntry;

typedef struct
{
    u32 unk0; // 0
    u32 unk1; // 4
    u32 unk2; // 8
    StageParamEntry *Entries; // C
} UiStageDatabase;

typedef struct
{
    void *FighterDatabase;
    UiStageDatabase *StageDatabase;
    void *Unk2Params;
    void *FigureDatabase;
} UiDatabaseManager;

UiDatabaseManager *UiDatabaseManager_instance;

StageParamEntry* getStageParamEntry(int stageId)
{
    UiDatabaseManager *uiDb = (UiDatabaseManager*)0x11FB7DC0;

    return &uiDb->StageDatabase->Entries[stageId];
}

void setStageBanState(int stageId, int stageType, int state)
{
    StageParamEntry *paramEntry = getStageParamEntry(stageId);

    if (stageType == 1)
        paramEntry->MaxFighters = state;
    else
        paramEntry->MaxFightersOmega = state;
}

void mOatlesTestThingLol()
{
    setStageBanState(0, 1, 0);
    setStageBanState(1, 1, 0);
    setStageBanState(2, 1, 0);
    setStageBanState(3, 1, 0);
    setStageBanState(4, 1, 0);
    setStageBanState(5, 1, 0);
}

int is_mii_character(void *uiCharacterDb, u8 id)
{
    return true;
}

void UseDynamicPortraits()
{
    // DetourFunction(0x030A028C, is_mii_character);

    // Battle portrait
    WriteCode(0x028F903C, 0x38600001); // Not entirely sure of the effects of this.
    WriteCode(0x028F9118, 0x38600001);
}

int smash_main(int argc, char **argv)
{
    mount_sd_fat("sd");

    u64 titleId = OSGetTitleID();

    // we only care about smash right now.
    if (titleId != SMASH_TITLEID_USA && titleId != SMASH_TITLEID_EUR && titleId != SMASH_TITLEID_JAP)
    {
        return entryPoint(argc, argv);
    }

    bool is117 = (*((u32*)0x0C282CEC) == 0x93C10010);

    applyModsFile(titleId);
    WriteCode(0x0292C5A0, asm_BLA(isNameAllowed));
    WriteCode(0x033F94F8, asm_BLA(isNameAllowed));
    // DetourFunction(0x033F3FE0, getRegionName);
    WriteCode(0x02C8814C, asm_B(0x140));
    UseDynamicPortraits();

    WriteCode(0x02C88148, asm_BLA(mOatlesTestThingLol));

    // // Force normal css for all modes (not at all useful)
    // WriteCode(0x02178C58, asm_LI(11, 1));

    // static const char *dumbShit = "DebugMeleeMenuScene2";
    // WriteCode(0x02C88920, asm_LIS(4, (u32)dumbShit >> 16));
    // WriteCode(0x02C88928, asm_ADDI(4, 4, (u32)dumbShit));

    /// If too many are enabled, the game won't start.
    /// Code size issue, afaik.
    // HookFighterStuff();
    // HookArticleStuff();
    // HookWeaponStuff();

    return entryPoint(argc, argv);
}
