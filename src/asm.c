#include <dynamic_libs/os_functions.h>
#include "asm.h"

u32 asm_B(u32 dest)
{
    return ((u32)dest & 0x03FFFFFC) | 0x48000000;
}

u32 asm_BL(void *dest)
{
    return ((u32)dest & 0x03FFFFFC) | 0x48000001;
}

u32 asm_BA(void *dest)
{
    return ((u32)dest & 0x03FFFFFC) | 0x48000002;
}

u32 asm_BLA(void *dest)
{
    return ((u32)dest & 0x03FFFFFC) | 0x48000003;
}

u32 asm_ADDIS(u32 destReg, u32 srcReg, u32 addr)
{
    u32 instr = 0x0F << 26;
    instr |= (destReg & 0x1F) << 21;
    instr |= (srcReg & 0x1F) << 16;
    instr |= (addr & 0xFFFF);
    
    return instr;
}

u32 asm_LIS(u32 r, u32 addr)
{
    return asm_ADDIS(r, 0, addr);
}

u32 asm_ADDI(u32 destReg, u32 srcReg, u32 addr)
{
    u32 instr = 0x0E << 26;
    instr |= (destReg & 0x1F) << 21;
    instr |= (srcReg & 0x1F) << 16;
    instr |= (addr & 0xFFFF);
    
    return instr;
}

u32 asm_ADDIC(u32 destReg, u32 srcReg, u32 addr)
{
    u32 instr = 0x0C << 26;
    instr |= (destReg & 0x1F) << 21;
    instr |= (srcReg & 0x1F) << 16;
    instr |= (addr & 0xFFFF);
    
    return instr;
}

u32 asm_LI(u32 r, u32 addr)
{
    return asm_ADDI(r, 0, addr);
}

u32 asm_NOP()
{
    return 0x60000000;
}

void WriteCode(u32 address, u32 instr)
{
    u32 *ptr = (u32 *)(address + 0xAA000000);
    *ptr = instr;
    DCFlushRange(ptr, 4);
    ICInvalidateRange(ptr, 4);
}

// lol the fuck it is
void DetourFunction(u32 address, void *replacementAddr)
{
    WriteCode(address, asm_BA(replacementAddr));

    // WriteCode(address+0x00, (((u32)replacementAddr >> 16) & 0xFFFF) | 0x3FE00000);
    // WriteCode(address+0x04, ((u32)replacementAddr & 0xFFFF) | 0x63FF0000);
    // WriteCode(address+0x08, 0x7FE903A6);
    // WriteCode(address+0x0C, 0x4E800421);
}
