#pragma once

s32 LoadFileToMem(const char *filepath, u8 **inbuffer, u32 *size);
s32 CheckFile(const char *filepath);
