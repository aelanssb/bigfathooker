#include <stddef.h>
#include <gctypes.h>
#include <dynamic_libs/os_functions.h>
#include <dynamic_libs/sys_functions.h>
#include <dynamic_libs/gx2_functions.h>
#include <dynamic_libs/fs_functions.h>
#include <dynamic_libs/vpad_functions.h>

#include "kernel/kernel.h"
#include "asm.h"
#include "smash.h"

void insertKernelBL(u32 addr, u32 target)
{
    u32 loc = (u32)OSEffectiveToPhysical((void*)addr);
    RunCodeAsKernel(&KernWritePhys, loc, asm_BL((void*)(target - addr)));
}

int __entry_menu(int argc, char **argv)
{
    InitOSFunctionPointers();
    InitSysFunctionPointers();
    InitGX2FunctionPointers();
    InitFSFunctionPointers();
    InitVPadFunctionPointers();

    // Redirect app main.
    insertKernelBL(0x0101C56C, (u32)smash_main);

    // Patch kernel address table to allow code writing.
    RunCodeAsKernel(&KernWritePhys, KERN_ADDRESS_TBL + 0x48, 0x80000000);
    RunCodeAsKernel(&KernWritePhys, KERN_ADDRESS_TBL + 0x4C, 0x28305800);

    // Start the damn game.
    if (SYSCheckTitleExists(SMASH_TITLEID_USA)) {
        SYSLaunchTitle(SMASH_TITLEID_USA);
    } else if (SYSCheckTitleExists(SMASH_TITLEID_EUR)) {
        SYSLaunchTitle(SMASH_TITLEID_EUR);
    } else if (SYSCheckTitleExists(SMASH_TITLEID_JAP)) {
        SYSLaunchTitle(SMASH_TITLEID_JAP);
    } else {
        _SYSLaunchTitleByPathFromLauncher("/vol/storage_odd03", 18, 0);
    }

    // EXIT_RELAUNCH_ON_LOAD
    return -3;
}
