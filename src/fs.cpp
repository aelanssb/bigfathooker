#include <libutils/fs/FSUtils.h>

extern "C" s32 LoadFileToMem(const char *filepath, u8 **buf, u32 *size)
{
    return FSUtils::LoadFileToMem(filepath, buf, size);
}

extern "C" s32 CheckFile(const char *filepath)
{
    return FSUtils::CheckFile(filepath);
}
